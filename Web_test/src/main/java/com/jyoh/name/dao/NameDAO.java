package com.jyoh.name.dao;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class NameDAO {
	
	@Inject
	SqlSessionTemplate template;
	
	public List<Map<String, Object>> list() throws Exception{
		return template.selectList("com.jyoh.name.dao.NameDAO.list");
	};
};